#include <math.h>
#include <GL/glut.h>
#include <SOIL/SOIL.h>

float xpos = 0, ypos = 0, zpos = 0, xrot = 0, yrot = 0, angle=0.0;
float cRadius = 10.0f;
float lastx, lasty;

float x_poskam= 15.0;
float z_poskam = 60.0;
float x_fokus = 15.0;
float z_fokus = 55.0;
float i=90;
float sudut;
float radius = 5;
float langlur = 0.5;
const double PI = 3.141592653589793;

GLfloat LightAmbient[] = {0.5f, 0.5f, 0.5f, 1.0f};
GLfloat LightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat LightPosition[] = {0.0f, 0.0f, 2.0f, 1.0f};

GLuint tex_2d;
GLuint tiang;
GLuint dunia;
GLuint rumput;
GLuint tembok;
GLuint gedungbilly;
GLuint dindingDaus;
GLuint atapDaus;
GLuint pintuDaus;
GLuint air;
GLuint temboksilinder;
GLuint pemandangan;
GLuint langit;

using namespace std;

GLuint LoadGLTextures(const char *filename) {
    tex_2d = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    glBindTexture(GL_TEXTURE_2D, tex_2d);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    return tex_2d;
}

GLuint LoadGLTextures2(const char *filename) {
    tex_2d = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,SOIL_FLAG_INVERT_Y);
    return tex_2d;
}

GLuint LoadGLTextures3(const char *filename) {
    tex_2d = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    glBindTexture(GL_TEXTURE_2D, tex_2d);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_REPEAT);
    return tex_2d;
}

void init (void) {
    tiang = LoadGLTextures("keramikputih.jpg");
    dunia = LoadGLTextures2("maps.jpg");
    rumput = LoadGLTextures3("rumput.jpg");
    tembok = LoadGLTextures3("tembokboy.png");
    gedungbilly = LoadGLTextures("tex.jpg");
    atapDaus = LoadGLTextures("atap.jpg");
    pintuDaus = LoadGLTextures("pintu.jpg");
    air = LoadGLTextures("air.jpg");
    temboksilinder = LoadGLTextures("tembokgedungsilinder.png");
    pemandangan = LoadGLTextures("wallpaper.png");
    langit = LoadGLTextures("langit.png");

    glEnable (GL_DEPTH_TEST);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
    glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
    glEnable (GL_COLOR_MATERIAL);
    glShadeModel (GL_SMOOTH);
}

// Daus
void gedung (void) {
        glPushMatrix(); //atap pintu
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, atapDaus);
        glTranslated(0,0.3,2.4);
        glScaled(3,0.2,9);
        glutSolidCube(1);
        glPopMatrix();

        glPushMatrix(); //kaki
        glTranslated(0,-1.8,0);
        glScaled(6.5,0.3,5.5);
        glutSolidCube(0.9);
        glPopMatrix();

        glPushMatrix(); //atap
        glTranslated(0,1.8,0);
        glScaled(6.5,0.3,5.5);
        glutSolidCube(0.9);
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();


        // belakang
        LoadGLTextures ("dinding.jpg");
        glEnable(GL_TEXTURE_2D);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.745, -1.89, -2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.745, 1.89,  -2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.745, 1.89,  -2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.745, -1.89, -2.295 );
        glEnd();

        // depan
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.745, 1.89,  2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.745, -1.89, 2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.745, -1.89, 2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.745, 1.89,  2.295 );
        glEnd();

        // kanan
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( 2.745,  -1.89, -2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f( 2.745,  1.89, -2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( 2.745,  1.89,  2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 2.745,  -1.89, 2.295);
        glEnd();

        // kiri
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( -2.745,  -1.89,  2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f( -2.745,  1.89,  2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.745,  1.89, -2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.745,  -1.89, -2.295 );
        glEnd();

        // atas
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.745,  1.89,  2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.745,  1.89, -2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.745,  1.89, -2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.745,  1.89,  2.295 );
        glEnd();

        // bawah
        glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.745,  -1.89, -2.295 );
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.745,  -1.89,  2.295 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.745,  -1.89,  2.295 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.745,  -1.89, -2.295 );
        glEnd();
        glDisable(GL_TEXTURE_2D);


        GLUquadricObj *cylinder;
        cylinder = gluNewQuadric();
        gluQuadricDrawStyle(cylinder, GLU_FILL);
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        gluQuadricTexture(cylinder, GL_TRUE);
        glBindTexture(GL_TEXTURE_2D, tiang);
        glRotatef(-90,1,0.0,0);
        glTranslated(2.7, -2.2, -1.8); //tabung depan kanan
        gluCylinder(cylinder, 0.2, 0.2, 3.7, 50, 1);
        glTranslated(-5.39, 0, 0); //tabung kiri depan
        gluCylinder(cylinder, 0.2, 0.2, 3.7, 50, 1);
        glTranslated(0, 4.40, 0); //tabung kanan belakang
        gluCylinder(cylinder, 0.2, 0.2, 3.7, 50, 1);
        glTranslated(5.40, 0, 0); //tabung kiri belakang
        gluCylinder(cylinder, 0.2, 0.2, 3.7, 50, 1);
        glTranslated(-2.7, -2.2, 3.7); //atap
        gluCylinder(cylinder, 2.2, 1.5, 0.6, 50, 1);
        glTranslated(0, 0, 0); //tiang atap
        gluCylinder(cylinder, 0.05, 0.05, 3, 50, 1);
        glTranslated(1.3, -3, -3.9); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glTranslated(-2.6, 0, 0); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glTranslated(0, -1.7, 0); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glTranslated(0, -1.7, 0); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glTranslated(2.6, 1.7, 0); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glTranslated(0, -1.7, 0); //pilar pintu
        gluCylinder(cylinder, 0.1, 0.1, 3, 50, 1);
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
}

void pintu(void){
     glPushMatrix(); //pintu
     glEnable(GL_TEXTURE_2D);
     glBindTexture(GL_TEXTURE_2D, pintuDaus);
     glTranslated(0,-0.9,2.4);
     glScaled(2,2,0.2);
     glutSolidCube(1);
     glDisable(GL_TEXTURE_2D);
     glPopMatrix();
}

void cemara(float x, float y, float z){
    glTranslatef(x,y,z);
    glScalef(2, 2, 2);
    GLUquadricObj *cylinder;
    glPushMatrix();
    glColor3ub(104,70,14);
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    glRotatef(-90,1,0.0,0);
    glTranslated(4, 2, -7); //batang pohon cemara
    gluCylinder(cylinder, 0.4, 0.1, 5, 360, 1);

    glColor3ub(18,118,13);
    glTranslated(0, 0, 2); //daun cemara
    gluCylinder(cylinder, 1, 0, 3, 360, 1);
    glTranslated(0, 0, 0.8);
    gluCylinder(cylinder, 0.9, 0, 2, 360, 1);
    glTranslated(0, 0, 0.8);
    gluCylinder(cylinder, 0.7, 0, 1.5, 360, 1);
    glTranslated(0, 0, 0.8);
    gluCylinder(cylinder, 0.5, 0, 1, 360, 1);
    glPopMatrix();
    glScalef(0.5, 0.5, 0.5);
    glTranslatef(0-x, 0-y, 0-z);
}

void pohon(float x, float y, float z){
    glTranslatef(x,y,z);
    GLUquadricObj *cylinder;

    glPushMatrix();
    glColor3f(0.4,0.27,0.055);
    cylinder = gluNewQuadric();
     gluQuadricDrawStyle(cylinder, GLU_FILL);
     glRotatef(-90,1,0.0,0);
     glTranslated(6, -6, -6); //batang pohon
     gluCylinder(cylinder, 0.4, 0.1, 5, 360, 1);
     glRotatef(-45,1,0.0,0); //ranting pohon belakang
     glTranslated(0, -2, 2);
     gluCylinder(cylinder, 0.1, 0.05, 2, 360, 1);
     glRotatef(45,0,1,1); //ranting pohon kiri
     glRotatef(-90,0,1,0); //ranting pohon kiri
     glTranslated(0, 0, 0);
     gluCylinder(cylinder, 0.1, 0.05, 2, 360, 1);
     glRotatef(-45,-1,-1,1); //ranting pohon kanan
     glRotatef(-45,-1,-1,0); //ranting pohon banan
     glTranslated(0, 0, 0);
     gluCylinder(cylinder, 0.1, 0.05, 3, 360, 1);
     glPopMatrix();

    glPushMatrix();
    glColor3ub(18,118,13);
    glScaled(1, 1, 1);
    glTranslatef(6,0,6);
    glutSolidDodecahedron(); //daun
    glScaled(0.5, 0.5, 0.5);
    glTranslatef(3,-1,2);
    glutSolidDodecahedron();
    glScaled(0.5, 0.5, 0.5);
    glTranslatef(-6,-4.5,-8);
    glutSolidDodecahedron();
    glScaled(2, 2, 2);
    glTranslatef(-3,1,2);
    glutSolidDodecahedron();
    glScaled(1, 1, 1);
    glTranslatef(1,1.5,2);
    glutSolidDodecahedron();
    glScaled(0.7, 0.7, 0.7);
    glTranslatef(1.5,-1.5,1);
    glutSolidDodecahedron();
    glScaled(0.6, 0.6, 0.6);
    glTranslatef(4,-0.2,-1);
    glutSolidDodecahedron();
    glScaled(2, 2, 2);
    glTranslatef(1,-1,-3);
    glutSolidDodecahedron();
    glScaled(1.2, 1.2, 1.2);
    glTranslatef(1,1,-2);
    glutSolidDodecahedron();
    glScaled(1, 1, 1);
    glTranslatef(-2,2,-1);
    glutSolidDodecahedron();
    glScaled(1, 1, 1);
    glTranslatef(-2,-2,0);
    glutSolidDodecahedron();
    glScaled(0.5, 0.5, 0.5);
    glTranslatef(1,1,1);
    glutSolidDodecahedron();
    glPopMatrix();
    glTranslatef(0-x,0-y,0-z);
}

void daus(){
     glLoadIdentity();

     glTranslatef(0.0f, 0.0f, -cRadius);
     glRotatef(xrot,1.0,0.0,0.0);

     glRotatef(yrot,0.0,1.0,0.0);
     glTranslated(-xpos,0.0f,-zpos);
     glColor3f(1.0f, 1.0f, 1.0f);
     glRotatef(-90,0.0,1.0,0.0);
     glScalef(0.7,0.7,0.7);
     glTranslatef(0,2,-17);
     gedung();
}

// Eric
void jalan(){
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -cRadius);
    glRotatef(xrot,1.0,0.0,0.0);
    glRotatef(yrot,0.0,1.0,0.0);
    glTranslated(-xpos,0.0f,-zpos);

    GLUquadricObj *cylinder;
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    LoadGLTextures ("aspal.png");
    glEnable(GL_TEXTURE_2D);
    glTranslatef(-5.3,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.3,  0.03,  1.2 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.3,  0.03, -1.2 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.3,  0.03, -1.2 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.3,  0.03,  1.2 );
    glEnd();
    glTranslatef(5.3,0,0);
    glTranslatef(6.5,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  3.7,  0.03,  1.2 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  3.7,  0.03, -1.2 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -3.7,  0.03, -1.2 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -3.7,  0.03,  1.2 );
    glEnd();
    glTranslatef(-6.5,0,0);
    glTranslatef(0,0,5.4);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  1.2,  0.03,  2.9 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  1.2,  0.03, -2.9 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -1.2,  0.03, -2.9 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -1.2,  0.03,  2.9 );
    glEnd();
    glTranslatef(0,0,-5.4);
    glTranslatef(0,0,-5.4);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  1.2,  0.03,  2.9 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  1.2,  0.03, -2.9 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -1.2,  0.03, -2.9 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -1.2,  0.03,  2.9 );
    glEnd();
    glTranslatef(0,0,5.4);

glRotatef(45, 0, 1, 0);
glTranslatef(-5.3,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  1.5,  0.03,  0.5 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  1.5,  0.03, -0.5 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -8.8,  0.03, -0.5 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -8.8,  0.03,  0.5 );
    glEnd();
    glTranslatef(5.3,0,0);
glRotatef(-45, 0, 1, 0);

glRotatef(45, 0, 1, 0);
glTranslatef(-5.3,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  9,  0.03,  0.5 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  9,  0.03, -0.5 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 19.5,  0.03, -0.5 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( 19.5,  0.03,  0.5 );
    glEnd();
    glTranslatef(5.3,0,0);
glRotatef(-45, 0, 1, 0);

glRotatef(-45, 0, 1, 0);
glTranslatef(-5.3,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  1.5,  0.03,  0.5 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  1.5,  0.03, -0.5 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( -8.8,  0.03, -0.5 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( -8.8,  0.03,  0.5 );
    glEnd();
    glTranslatef(5.3,0,0);
glRotatef(45, 0, 1, 0);

glRotatef(-45, 0, 1, 0);
glTranslatef(-5.3,0,0);
    glBegin(GL_POLYGON);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(  9,  0.03,  0.5 );
        glTexCoord2f(0.0f, 1.0f); glVertex3f(  9,  0.03, -0.5 );
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 19.5,  0.03, -0.5 );
        glTexCoord2f(1.0f, 0.0f); glVertex3f( 19.5,  0.03,  0.5 );
    glEnd();
    glTranslatef(5.3,0,0);
glRotatef(45, 0, 1, 0);

    glTranslatef(0,0.05,0);
    glRotatef(90,1,0,0);
    gluQuadricTexture(cylinder, GL_TRUE);
    gluDisk(cylinder, 2.7, 4, 50, 1);
    gluDisk(cylinder, 14, 15.5, 50, 1);
    glRotatef(-90,1,0,0);
    glTranslatef(0,-0.05,0);
    glDisable(GL_TEXTURE_2D);
}

void kubuseric(float rotasi, float tinggi){
    glTranslatef(0, 0, tinggi);
    glRotatef( rotasi, 0.0, 0.0, 1.0 );

    // bawah
    glBegin(GL_POLYGON);
    glVertex3f(  2.5, -2.5, 0 );
    glVertex3f(  2.5,  2.5, 0 );
    glVertex3f( -2.5,  2.5, 0 );
    glVertex3f( -2.5, -2.5, 0 );
    glEnd();

    // atas
    glBegin(GL_POLYGON);
    glColor3f(1,1,1);
    glVertex3f(  2.5, -2.5  , 1.25 );
    glVertex3f(  2.5,  2.5, 1.25 );
    glVertex3f( -2.5,  2.5, 1.25 );
    glVertex3f( -2.5, -2.5  , 1.25 );
    glEnd();

    // kiri
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 1.0f); glVertex3f( 2.5, -2.5  , 0 );
    glTexCoord2f(0.0f, 0.0f); glVertex3f( 2.5,  2.5, 0 );
    glTexCoord2f(1.0f, 0.0f); glVertex3f( 2.5,  2.5, 1.25 );
    glTexCoord2f(1.0f, 1.0f); glVertex3f( 2.5, -2.5  , 1.25 );
    glEnd();

    // kanan
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 1.0f); glVertex3f( -2.5, -2.5  , 1.25 );
    glTexCoord2f(0.0f, 0.0f); glVertex3f( -2.5,  2.5, 1.25 );
    glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.5,  2.5, 0 );
    glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.5, -2.5  , 0 );
    glEnd();

    // depan
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.5,  2.5, 1.25 );
    glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.5,  2.5, 0 );
    glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.5,  2.5, 0 );
    glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.5,  2.5, 1.25 );
    glEnd();

    // belakang
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(  2.5,  -2.5, 0 );
    glTexCoord2f(0.0f, 0.0f); glVertex3f(  2.5,  -2.5,  1.25 );
    glTexCoord2f(1.0f, 0.0f); glVertex3f( -2.5,  -2.5,  1.25 );
    glTexCoord2f(1.0f, 1.0f); glVertex3f( -2.5,  -2.5, 0 );
    glEnd();

    glRotatef( 0-rotasi, 0.0, 0.0, 1.0 );
    glTranslatef(0, 0, 0-tinggi);
}

void pesawat(){
    glTranslatef(0,0,15);
    //glLoadIdentity();
    glPushMatrix();
    GLUquadricObj *cylinder;
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    glRotatef(-90,1,0,0);
    glTranslatef(0,-6,0);
    gluCylinder(cylinder, 0.2, 0.5, 3, 50, 1); //badan
    //glRotatef(180,1,0,0);
    glTranslatef(0,0,3);
    gluCylinder(cylinder, 0.5, 0.1, 0.8, 50, 1); //moncong
    glTranslatef(0,0,-3);
    glRotatef(90,1,0,0);

    //sayap kanan
    glTranslatef( 0.8, 2, -0.5);
    glBegin(GL_POLYGON);
    glVertex3f( 3, 0, 0.5 );
    glVertex3f(-0.5, 1.0, 0.5 );
    glVertex3f(-0.5, 0.0, 0.5 );
    glEnd();
    glTranslatef( -0.8, -2, 0.5);

    //sayap kiri
    glTranslatef( -0.8, 2, -0.5);
    glBegin(GL_POLYGON);
    glVertex3f( -3, 0, 0.5 );
    glVertex3f(0.5, 1.0, 0.5 );
    glVertex3f(0.5, 0.0, 0.5 );
    glEnd();
    glTranslatef( 0.8, -2, 0.5);

    //sayap belakang
    //kanan
    glTranslatef( -0.3, 0, -0.5);
    glBegin(GL_POLYGON);
    glVertex3f( 1.5, 0, 0.5 );
    glVertex3f(0.5, 1.0, 0.5 );
    glVertex3f(0.5, 0.0, 0.5 );
    glEnd();
    glTranslatef( 0.3, 0, 0.5);

    //kiri
    glRotatef(180,0,1,0);
    glTranslatef( -0.3, 0, -0.5);
    glBegin(GL_POLYGON);
    glVertex3f( 1.5, 0, 0.5 );
    glVertex3f(0.5, 1.0, 0.5 );
    glVertex3f(0.5, 0.0, 0.5 );
    glEnd();
    glTranslatef( 0.3, 0, 0.5);
    glRotatef(-180,0,1,0);

    //atas
    glRotatef(-90,0,1,0);
    glTranslatef( -0.3, 0, -0.5);
    glBegin(GL_POLYGON);
    glVertex3f( 1.5, 0, 0.5 );
    glVertex3f(0.5, 1.0, 0.5 );
    glVertex3f(0.5, 0.0, 0.5 );
    glEnd();
    glTranslatef( 0.3, 0, 0.5);
    glRotatef(-90,0,1,0);
}

void eric(void){
    glLoadIdentity();

    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(0.0f, 0.0f, -cRadius);
    glRotatef(xrot,1.0,0.0,0.0);
    glRotatef(yrot,0.0,1.0,0.0);
    glTranslated(-xpos,0.0f,-zpos);
    glRotatef(-90,1,0,0);
    glPushMatrix();
    GLUquadricObj *cylinder;
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    LoadGLTextures("kaca.png");
    glEnable(GL_TEXTURE_2D);
    glTranslatef(0,-10,0);
    glScalef(0.7,0.7,0.7);
    kubuseric(0,0);
    float tinggi = 1.25;
    int rotasi = 18;

    for (int i=0;i<4;i++)
    {
        kubuseric(rotasi,tinggi);
        glRotatef( rotasi, 0.0, 0.0, 1.0 );
        glTranslatef(0, 0, -2.25);

        glTranslatef(2.3, 2.3, 2.3);
        gluCylinder(cylinder, 0.2, 0.2, tinggi, 50, 1);
        glTranslatef(-2.3, -2.3, -2.3);

        glTranslatef(2.3, -2.3, 2.3);
        gluCylinder(cylinder, 0.2, 0.2, tinggi, 50, 1);
        glTranslatef(-2.3, 2.3, -2.3);

        glTranslatef(-2.3, 2.3, 2.3);
        gluCylinder(cylinder, 0.2, 0.2, tinggi, 50, 1);
        glTranslatef(2.3, -2.3, -2.3);

        glTranslatef(-2.3, -2.3, 2.3);
        gluCylinder(cylinder, 0.2, 0.2, tinggi, 50, 1);
        glTranslatef(2.3, 2.3, -2.3);

        glTranslatef(0, 0, 2.25);
        glRotatef( 0-rotasi, 0.0, 0.0, 1.0 );

        tinggi = tinggi + 1.25;
        rotasi = rotasi + 18;
    }
    glDisable(GL_TEXTURE_2D);
    glTranslatef(0,10,0);
}

// Billy
void tiangbilly()
{
    GLUquadricObj *cylinder;
    glPushMatrix();
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    gluCylinder(cylinder, 0.2, 0.2, 1, 50, 1);
    glPopMatrix();
}

void kubusbilly()
{
      // depan
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(0.5, 0.0, -0.5 );
      glTexCoord2f(0.0f, 0.0f); glVertex3f(0.5, 1.0, -0.5 );
      glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5, 1.0, -0.5 );
      glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5, 0.0, -0.5 );
      glEnd();

      // belakang
      glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(0.5, 0.0, 0.5 );
      glTexCoord2f(0.0f, 0.0f); glVertex3f(0.5, 1.0, 0.5 );
      glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5, 1.0, 0.5 );
      glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5, 0.0, 0.5 );
      glEnd();

      // kanan
      glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(0.5, 0.0, -0.5 );
      glTexCoord2f(0.0f, 0.0f); glVertex3f(0.5, 1.0, -0.5 );
      glTexCoord2f(1.0f, 0.0f); glVertex3f(0.5, 1.0, 0.5 );
      glTexCoord2f(1.0f, 1.0f); glVertex3f(0.5, 0.0, 0.5 );
      glEnd();

      // kiri
      glBegin(GL_POLYGON);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5, 0.0, 0.5 );
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5, 1.0, 0.5 );
      glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5, 1.0, -0.5 );
      glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5, 0.0, -0.5 );
      glEnd();

      // atas
      glBegin(GL_POLYGON);
      glVertex3f(0.5, 1.0, 0.5 );
      glVertex3f(0.5, 1.0, -0.5 );
      glVertex3f(-0.5, 1.0, -0.5 );
      glVertex3f(-0.5, 1.0, 0.5 );
      glEnd();

      // bawah
      glBegin(GL_POLYGON);
      glVertex3f(0.5, 0.0, -0.5 );
      glVertex3f(0.5, 0.0, 0.5 );
      glVertex3f(-0.5, 0.0, 0.5 );
      glVertex3f(-0.5, 0.0, -0.5 );
      glEnd();
}

void pohonbilly(){
    GLUquadricObj *cylinder;
    glPushMatrix();
    cylinder = gluNewQuadric();
    gluQuadricDrawStyle(cylinder, GLU_FILL);

    // batang pohon
    glColor3ub (245, 61, 0);
    glTranslated (4.0f, 0.0f, 4.0f);
    glRotatef(-90,1.0,0.0,0.0);
        gluCylinder(cylinder, 0.1, 0.06, 1.1, 50, 1);
    glRotatef(90,1.0,0.0,0.0);
    glTranslated (-4.0f, 0.0f, -4.0f);
    glColor3ub (255, 255, 255);

    // dedaunan
    glTranslated (4.0f, 0.0f, 4.0f);
    glRotatef(-90,1.0,0.0,0.0);
    glColor3f (0.0f, 1.0f, 0.0f);
    glTranslated (0.0f, 0.0f, 1.0f);

    glTranslated (0.0f, 0.0f, 0.05f);
        gluCylinder(cylinder, 0.2, 0.2, 0.9, 50, 1);//silinder 1
        gluDisk(cylinder, 0, 0.2, 50, 1);           //tutup 1.1
    glTranslated (0.0f, 0.0f, -0.05f);
    glTranslated (0.0f, 0.0f, 0.95f);
        gluDisk(cylinder, 0, 0.2, 50, 1);           //tutup 1.2
    glTranslated (0.0f, 0.0f, -0.95f);

    glTranslated (0.0f, 0.0f, 0.1f);
        gluCylinder(cylinder, 0.3, 0.3, 0.8, 50, 1);//silinder 2
        gluDisk(cylinder, 0, 0.3, 50, 1);           //tutup 2.1
    glTranslated (0.0f, 0.0f, -0.1f);
    glTranslated (0.0f, 0.0f, 0.9f);
        gluDisk(cylinder, 0, 0.3, 50, 1);           //tutup 2.2
    glTranslated (0.0f, 0.0f, -0.9f);

    glTranslated (0.0f, 0.0f, 0.15f);
        gluCylinder(cylinder, 0.4, 0.4, 0.7, 50, 1);//silinder 3
        gluDisk(cylinder, 0, 0.4, 50, 1);           //tutup 3.1
    glTranslated (0.0f, 0.0f, -0.15f);
    glTranslated (0.0f, 0.0f, 0.85f);
        gluDisk(cylinder, 0, 0.4, 50, 1);           //tutup 3.2
    glTranslated (0.0f, 0.0f, -0.85f);

    glTranslated (0.0f, 0.0f, 0.25f);
        gluCylinder(cylinder, 0.45, 0.45, 0.5, 50, 1);//silinder 4
        gluDisk(cylinder, 0, 0.45, 50, 1);          //tutup 4.1
    glTranslated (0.0f, 0.0f, -0.25f);
    glTranslated (0.0f, 0.0f, 0.75f);
        gluDisk(cylinder, 0, 0.45, 50, 1);          //tutup 4.2
    glTranslated (0.0f, 0.0f, -0.75f);

    glTranslated (0.0f, 0.0f, 0.3f);
        gluCylinder(cylinder, 0.35, 0.35, 0.4, 50, 1);//silinder 5
        gluDisk(cylinder, 0, 0.35, 50, 1);          //tutup 5.1
    glTranslated (0.0f, 0.0f, -0.3f);
    glTranslated (0.0f, 0.0f, 0.7f);
        gluDisk(cylinder, 0, 0.35, 50, 1);          //tutup 5.2
    glTranslated (0.0f, 0.0f, -0.7f);

    glTranslated (0.0f, 0.0f, 0.4f);
        gluCylinder(cylinder, 0.5, 0.5, 0.2, 50, 1);//silinder 6
        gluDisk(cylinder, 0, 0.5, 50, 1);           //tutup 6.1
    glTranslated (0.0f, 0.0f, -0.4f);
    glTranslated (0.0f, 0.0f, 0.6f);
        gluDisk(cylinder, 0, 0.5, 50, 1);           //tutup 6.2
    glTranslated (0.0f, 0.0f, -0.6f);

    glTranslated (0.0f, 0.0f, -1.0f);
    glColor3f (1.0f, 1.0f, 1.0f);
    glRotatef(90,1.0,0.0,0.0);
    glTranslated (-4.0f, 0.0f, -4.0f);
    glPopMatrix();
}

void billy(){
    //glLoadIdentity();

    //blok 1
    glTranslated (0.0f, 0.0f, 6.0f);    //awal 1.1
    glScaled (1.0f, 1.4f, 1.0f);
    glColor3f (1.0f, 1.0f, 1.0f);
    glEnable (GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, gedungbilly);
    kubusbilly ();
    glTranslated (0.0f, 0.0, -6.0f);    //reposisi 1.1
    glScaled (1.0f, 0.714f, 1.0f);

    //blok 2
    glTranslated (0.0f, 1.4f, 6.0f);    //awal 2.1
    glScaled (1.0f, 1.4f, 1.0f);
    glTranslated (0.05f, 0.0f, 0.05f);  //awal 2.2
    glScaled (0.9f, 0.9f, 0.9f);
    glColor3f (1.0f, 1.0f, 1.0f);
    kubusbilly ();
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 2.2
    glTranslated (-0.05f, 0.0f, -0.05f);
    glScaled (1.0f, 0.714f, 1.0f);      //reposisi 2.1
    glTranslated (0.0f, -1.4f, -6.0f);


    //blok 3
    glTranslated (0.0f, 1.4f, 6.0f);    //awal 3.1
    glScaled (1.0f, 1.4f, 1.0f);
    glTranslated (0.05f, 0.0f, 0.05f);  //awal 3.2
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, -0.05f);//awal 3.3
    glScaled (0.9f, 0.9f, 0.9f);
    glColor3f (1.0f, 1.0f, 1.0f);
    kubusbilly ();
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 3.3
    glTranslated (0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 3.2
    glTranslated (-0.05f, 0.0f, -0.05f);
    glScaled (1.0f, 0.714f, 1.0f);      //reposisi 3.1
    glTranslated (0.0f, -1.4f, -6.0f);


    //blok 4
    glTranslated (0.0f, 1.4f, 6.0f);    //awal 4.1
    glScaled (1.0f, 1.4f, 1.0f);
    glTranslated (0.05f, 0.0f, 0.05f);  //awal 4.2
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, -0.05f);//awal 4.3
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, 0.05f); //awal 4.4
    glScaled (0.9f, 0.8f, 0.9f);
    glColor3f (1.0f, 1.0f, 1.0f);
    kubusbilly ();
    glScaled (1.111f, 1.25f, 1.111f);   //reposisi 4.4
    glTranslated (0.05f, -1.0, -0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 4.3
    glTranslated (0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 4.2
    glTranslated (-0.05f, 0.0f, -0.05f);
    glScaled (1.0f, 0.714f, 1.0f);      //reposisi 4.1
    glTranslated (0.0f, -1.4f, -6.0f);


    //blok 5
    glTranslated (0.0f, 1.4f, 6.0f);    //awal 5.1
    glScaled (1.0f, 1.4f, 1.0f);
    glTranslated (0.05f, 0.0f, 0.05f);  //awal 5.2
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, -0.05f);//awal 5.3
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, 0.05f); //awal 5.4
    glScaled (0.9f, 0.8f, 0.9f);
    glTranslated (0.05f, 1.0f, -0.05f); //awal 5.5
    glScaled (0.9f, 0.7f, 0.9f);
    glColor3f (1.0f, 1.0f, 1.0f);
    kubusbilly ();
    glScaled (1.111f, 1.428f, 1.111f);  //reposisi 5.5
    glTranslated (-0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.25f, 1.111f);   //reposisi 5.4
    glTranslated (0.05f, -1.0, -0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 5.3
    glTranslated (0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 5.2
    glTranslated (-0.05f, 0.0f, -0.05f);
    glScaled (1.0f, 0.714f, 1.0f);      //reposisi 5.1
    glTranslated (0.0f, -1.4f, -6.0f);
    glDisable(GL_TEXTURE_2D);

    //antena
    glTranslated (0.0f, 1.4f, 6.0f);    //awal 6.1
    glScaled (1.0f, 1.4f, 1.0f);
    glTranslated (0.05f, 0.0f, 0.05f);  //awal 6.2
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, -0.05f);//awal 6.3
    glScaled (0.9f, 0.9f, 0.9f);
    glTranslated (-0.05f, 1.0f, 0.05f); //awal 6.4
    glScaled (0.9f, 0.8f, 0.9f);
    glTranslated (0.05f, 1.0f, -0.05f); //awal 6.5
    glScaled (0.9f, 0.7f, 0.9f);
    glTranslated (0.0f, 2.5f, 0.0f);    //awal 6.6
    glRotatef(90, 1.0f, 0.0f , 0.0f);
    glScaled (0.1f, 0.1f, 1.5f);
    glColor3f(0.0f, 0.0f, 0.0f);
    tiangbilly ();
    glScaled (10.0f, 10.0f, 0.666f);        //reposisi 6.6
    glRotatef(-90, 1.0f, 0.0f , 0.0f);
    glTranslated (0.0f, -2.5f, 0.0f);
    glScaled (1.111f, 1.428f, 1.111f);  //reposisi 6.5
    glTranslated (-0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.25f, 1.111f);   //reposisi 6.4
    glTranslated (0.05f, -1.0, -0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 6.3
    glTranslated (0.05f, -1.0f, 0.05f);
    glScaled (1.111f, 1.111f, 1.111f);  //reposisi 6.2
    glTranslated (-0.05f, 0.0f, -0.05f);
    glScaled (1.0f, 0.714f, 1.0f);      //reposisi 6.1
    glTranslated (0.0f, -1.4f, -6.0f);
}

// Boy
void lapangan(){
    glPushMatrix();
        GLUquadricObj *cylinder;
        cylinder = gluNewQuadric();
        gluQuadricNormals(cylinder, GLU_SMOOTH);
        gluQuadricDrawStyle(cylinder, GLU_FILL);
        glColor3f(0.0f, 0.5f, 0.0f);
        glEnable(GL_TEXTURE_2D);
            gluQuadricTexture(cylinder, GL_TRUE);
            glBindTexture(GL_TEXTURE_2D, rumput);
            gluDisk(cylinder, 0, 15, 50, 1);
        glDisable(GL_TEXTURE_2D);
        glTranslatef(0.0,0.0,-0.2);
        gluCylinder(cylinder, 15, 15, 0.2, 50, 1);
        gluDisk(cylinder, 0, 15, 50, 1);
        glTranslatef(0.0,0.0,0.2);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();
}

void awan() {
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glColor3f(0.6f, 0.87f, 1.0f);

    glPushMatrix();
        glTranslatef(-5.0,24.0,11.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-5.0,24.0,11.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-5.0,24.0,9.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-2.0,24.0,7.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-2.0,24.0,5.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(1.0,24.0,5.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(4.0,24.0,8.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(4.0,24.0,8.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(4.0,24.0,7.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(4.0,24.0,11.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(4.0,24.0,7.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(3.0,24.0,5.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(3.0,21.0,3.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(6.0,18.0,-6.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-3.0,15.0,6.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(8.0,12.0,8.0);

        glutSolidSphere(1, 50, 50);
        glTranslatef(0.5,0,0.5);
        glutSolidSphere(0.5, 50, 50);
        glTranslatef(-0.5,0,-0.5);
        glTranslatef(-0.4,-0.2,-0.4);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(0.4,0.2,0.4);
        glTranslatef(0.8,0.0,0.0);
        glutSolidSphere(0.7, 50, 50);
        glTranslatef(-0.8,0.0,0.0);
        glTranslatef(0.3,-0.2,-0.9);
        glutSolidSphere(0.6, 50, 50);
        glTranslatef(-0.6,0.2,-0.2);
    glPopMatrix();

    glColor3f(1.0f, 1.0f, 1.0f);
}

void globe(){
    GLUquadricObj *Bumi;
    Bumi = gluNewQuadric();
    gluQuadricNormals(Bumi, GLU_SMOOTH);
    gluQuadricTexture(Bumi, GL_TRUE);
    gluCylinder(Bumi, 0.5, 0.0, 1, 6, 1);
    glPushMatrix();
        glTranslatef(0.0,0.0,2.0);
        glRotatef(-15.0, 1.0, 0.0, 0.0);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, dunia);
        gluSphere(Bumi, 1.7, 50, 180);
        glDisable(GL_TEXTURE_2D);
        glRotatef(15.0, 1.0, 0.0, 0.0);
        glTranslatef(0.0,0.0,-2.0);

        GLUquadricObj *cylinder;
        cylinder = gluNewQuadric();
        gluQuadricDrawStyle(cylinder, GLU_FILL);
        gluCylinder(cylinder, 2.7, 2.7, 0.2, 50, 1);
        gluCylinder(cylinder, 2.5, 2.5, 0.2, 50, 1);
            glTranslatef(0.0,0.0,0.2);
            gluDisk(cylinder, 2.5, 2.7, 50, 1);
            glTranslatef(0.0,0.0,-0.1);
            glColor3f(0.0f, 0.5f, 0.9f);
            glEnable(GL_TEXTURE_2D);
            gluQuadricTexture(cylinder, GL_TRUE);
            glBindTexture(GL_TEXTURE_2D,air);
            gluDisk(cylinder, 0, 2.5, 50, 1);
            glDisable(GL_TEXTURE_2D);
            glColor3f(1.0f, 1.0f, 1.0f);
            glTranslatef(0.0,0.0,-0.1);
    glPopMatrix();
}

void boy(){
    //Alas dan atap gedung
    glPushMatrix();
        GLUquadricObj *cylinder;
        cylinder = gluNewQuadric();
        gluQuadricDrawStyle(cylinder, GLU_FILL);
        gluQuadricTexture(cylinder, GL_TRUE);
        gluCylinder(cylinder, 2.7, 2.7, 0.5, 50, 1);

        //Transparansi
        //glColor4f(0.0f, 0.5f, 0.9f, 0.50f);
        //glEnable(GL_BLEND);
        //glBlendFunc(GL_ONE_MINUS_DST_ALPHA,GL_DST_ALPHA);
        //glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        //glBlendFunc(GL_SRC_ALPHA,GL_ONE);

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, temboksilinder);
        gluCylinder(cylinder, 2.8, 2.8, 4.2, 50, 1);
        glDisable(GL_TEXTURE_2D);

        //glDisable(GL_BLEND);

        glColor3f(1.0f, 1.0f, 1.0f);
        glEnable(GL_TEXTURE_2D);
        gluQuadricTexture(cylinder, GL_TRUE);

        glBindTexture(GL_TEXTURE_2D, tembok);
        gluCylinder(cylinder, 2.9, 2.9, 0.5, 50, 1);
        glDisable(GL_TEXTURE_2D);

        glTranslated(0.0,0.0,0.5);
        gluDisk(cylinder, 2.7, 2.9, 50, 1);
        glTranslated(0.0,0.0,3.5);
        gluDisk(cylinder, 2.3, 2.9, 50, 1);

        glColor3f(0.0, 0.5, 0.9);
        gluCylinder(cylinder, 2.9, 2.9, 0.2, 50, 1);
        glTranslated(0.0,0.0,0.2);
        gluDisk(cylinder, 0, 2.9, 50, 1);
        glColor3f(1.0f, 1.0f, 1.0f);

        glTranslated(0.0,0.0,-1);

        glEnable(GL_TEXTURE_2D);
        glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
        glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
        glEnable(GL_TEXTURE_GEN_S);
        glEnable(GL_TEXTURE_GEN_T);
        glBindTexture(GL_TEXTURE_2D, langit);
        gluSphere(cylinder, 2, 50, 180);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_TEXTURE_GEN_S);
        glDisable(GL_TEXTURE_GEN_T);
        glTranslated(0.0,0.0,1);
    glPopMatrix();

    //Pilar
    glPushMatrix();
        GLUquadricObj *pilar;
        pilar = gluNewQuadric();
        gluQuadricNormals(pilar, GLU_SMOOTH);
        gluQuadricTexture(pilar, GL_TRUE);
        gluQuadricDrawStyle(pilar, GLU_FILL);
        glTranslated(0.0,2.8,0.0);

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tiang);

        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(0.0,-5.6,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-2.8,2.8,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        //Kembali ke titik pusat
        glTranslated(2.8,0.0,0.0);

        glRotatef(45.0, 0.0, 0.0, 1.0);
        glTranslated(2.8,0.0,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-2.8,2.8,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(0.0,-5.6,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-2.8,2.8,0.0);
        gluCylinder(pilar, 0.3, 0.3, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.3, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        //Kembali ke titik pusat
        glTranslated(2.8,0.0,0.0);
        glRotatef(-45.0, 0.0, 0.0, 1.0);

        glDisable(GL_TEXTURE_2D);

        glColor3f(0.0f, 0.5f, 0.9f);
        glTranslated(0.0,3.1,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(0.0,-6.2,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-3.1,3.1,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        //Kembali ke titik pusat
        glTranslated(3.1,0.0,0.0);

        glRotatef(45.0, 0.0, 0.0, 1.0);
        glTranslated(3.1,0.0,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-3.1,3.1,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(0.0,-6.2,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        glTranslated(-3.1,3.1,0.0);
        gluCylinder(pilar, 0.05, 0.05, 4.3, 50, 1);
            glTranslated(0.0,0.0,4.3);
            gluDisk(cylinder, 0, 0.05, 50, 1);
            glTranslated(0.0,0.0,-4.3);
        //Kembali ke titik pusat
        glTranslated(3.1,0.0,0.0);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();

    //Panggung
    glPushMatrix();
        GLUquadricObj *cylinder2;
        cylinder2 = gluNewQuadric();
        gluQuadricDrawStyle(cylinder2, GLU_FILL);

        //Lingkaran panggung
        gluCylinder(cylinder2, 0.5, 0.5, 0.2, 50, 1);
            glTranslated(0.0,0.0,0.2);
            gluDisk(cylinder2, 0, 0.5, 50, 1);
            glTranslated(0.0,0.0,-0.2);

        //Persegi panjang panggung
        glScalef(10.0,15.0,1.0);
        glTranslated(-0.1,0.0,0.1);
        glutSolidCube(0.2);
        glTranslated(0.1,0.0,-0.1);
        glScalef(-10.0,-15.0,1.0);
    glPopMatrix();

    //Monitor panggung
    glPushMatrix();
        glScalef(1.0,12.0,10.0);
        glTranslated(-1.8,0.0,0.12);
        glColor3f(0.0f, 0.0f, 0.0f);
        glutSolidCube(0.2);
        glColor3f(1.0f, 1.0f, 1.0f);
        glTranslated(1.8,0.0,-0.12);
        glScalef(1.0,-12.0,-10.0);
    glPopMatrix();

    //Pintu Aula
    glPushMatrix();
        glTranslated(2.8,0.0,0.2);
        glScalef(0.6,1.1,1.0);
        glColor3f(0.0, 0.5, 0.9);
        glutSolidCube(0.4);
        glColor3f(1.0f, 1.0f, 1.0f);
    glPopMatrix();
}

void reshape (int w, int h) {
    glViewport (0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode (GL_PROJECTION);

    glLoadIdentity ();
    gluPerspective (60, (GLfloat)w / (GLfloat)h, 0.1, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

void display (void) {
    glClearColor (1.0,1.0,1.0,1.0);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    glTranslatef(0.0f, 0.0f, -cRadius);
    glRotatef(xrot,1.0,0.0,0.0);
    glRotatef(yrot,0.0,1.0,0.0);
    glTranslated(-xpos,0.0f,-zpos);

    // Boy
    glRotatef(-90,1.0,0.0,0.0);
    lapangan();
    glRotatef(-90,0.0,0.0,1.0);
    glTranslated(-10.0,0.0,0.0);
    boy();
    glTranslated(10.0,0.0,0.0);
    globe();
    glRotatef(90,1.0,0.0,0.0);
    awan();

    // Billy
    glScaled(5,5,5);
    glTranslated(0,0,-4);
    billy();
    glTranslated(0,0,4);
    glScaled(0.2,0.2,0.2);

    glTranslated(0,0,2);
        pohonbilly();
    glTranslated(0,0,-2);

    glTranslated(6,0,2);
        pohonbilly();
    glTranslated(-6,0,-2);

    glTranslated(0,0,-6);
        pohonbilly();
    glTranslated(0,0,6);

    glTranslated(-8,0,-6);
        pohonbilly();
    glTranslated(8,0,6);

    glTranslated(-10,0,-2);
        pohonbilly();
    glTranslated(10,0,2);

    glTranslated(-12,0,6);
        pohonbilly();
    glTranslated(12,0,-6);

    glTranslated(2,0,-14);
        pohonbilly();
    glTranslated(-2,0,14);

    glTranslated(-7,0,-10);
        pohonbilly();
    glTranslated(7,0,10);

    // Daus
    glScalef(0.125,0.125,0.125);
    pohon(40,6,-66);
    pohon(77,6,-56);

    pohon(-40,6,15);
    pohon(-40,6,77);

    pohon(-40,6,-55);
    pohon(-70,6,-35);

    pohon(55,6,35);
    pohon(30,6,78);

    cemara(10,13,-40);
    cemara(80,13,-30);

    cemara(-35,13,47);
    cemara(-79,13,38);

    cemara(-40,13,-70);
    cemara(-70,13,-45);

    cemara(30,13,19);
    cemara(50,13,75);

    glScalef(8,8,8);
    daus();
    pintu();

    // Eric
    eric();
    pesawat();
    jalan();

    glFlush();
    glutSwapBuffers();
}

void keyboard (unsigned char key, int x, int y) {
    if (key=='s')
    {
        xrot += 1;
        if (xrot >360)
            xrot -= 360;
    }

    if (key=='w')
    {
        xrot -= 1;
        if (xrot < -360)
            xrot += 360;
    }

    if (key=='q')
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos += float(sin(yrotrad));
        zpos -= float(cos(yrotrad));
        ypos -= float(sin(xrotrad));
    }

    if (key=='e')
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos -= float(sin(yrotrad));
        zpos += float(cos(yrotrad));
        ypos += float(sin(xrotrad));
    }

    if (key=='d')
    {
        float yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xpos += float(cos(yrotrad)) * 0.2;
        zpos += float(sin(yrotrad)) * 0.2;
    }

    if (key=='a')
    {
        float yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xpos -= float(cos(yrotrad)) * 0.2;
        zpos -= float(sin(yrotrad)) * 0.2;
    }

    if (key==27)
    {
        exit(0);
    }
}

void mouseMovement(int x, int y) {
    int diffx=x-lastx;
    int diffy=y-lasty;
    lastx=x;
    lasty=y;
    xrot += (float) diffy;
    yrot += (float) diffx;
}

int main(int argc, char *argv[])
{
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (1000, 600);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Grafika Komputer");
    init ();
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutReshapeFunc (reshape);

    glutPassiveMotionFunc(mouseMovement);
    glutKeyboardFunc (keyboard);

    glutMainLoop ();
    return 0;
}





